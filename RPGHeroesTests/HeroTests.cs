using RPGHeroes.Attributes;
using RPGHeroes.Items;
using RPGHeroes.Items.Armors;
using RPGHeroes.Items.Weapons;
using System.Reflection.Emit;
using System.Text;
using System.Xml.Linq;

namespace RPGHeroesTests
{
    public class HeroTests
    {
        [Fact]
        public void Create_Hero_ShouldHaveNameJeff()
        {
            //Arrange
            RPGHeroes.Heroes.Hero hero = new RPGHeroes.Heroes.Warrior("Jeff");
            string expected = "Jeff";
            //Act & Assert
            hero.Name = expected;
        }

        [Fact]
        public void Equip_WarriorValidWeapon_ShouldEquipAxe()
        {
            //Arrange
            RPGHeroes.Heroes.Hero hero = new RPGHeroes.Heroes.Warrior("Jeff");
            RPGHeroes.Items.Item axe = new RPGHeroes.Items.Weapons.Weapon("Common Axe", 1, WeaponType.Axe, 2);
            RPGHeroes.Items.Slot weaponSlot = RPGHeroes.Items.Slot.Weapon;
            RPGHeroes.Items.Item expected = new RPGHeroes.Items.Weapons.Weapon("Common Axe", 1, WeaponType.Axe, 2);
            //Act
            hero.Equip(axe);
            RPGHeroes.Items.Item actual = hero.Equipment[weaponSlot];
            //Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void Equip_WarriorInvalidWeapon_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            RPGHeroes.Heroes.Hero hero = new RPGHeroes.Heroes.Warrior("Jeff");
            RPGHeroes.Items.Item staff = new RPGHeroes.Items.Weapons.Weapon("Common Staff", 1, WeaponType.Staff, 2);
            string expected = "Invalid weapon for this Hero class.";
            //Act & Assert
            Exception exception = Assert.Throws<InvalidWeaponException> (() => hero.Equip(staff));
            Assert.Equal(expected, exception.Message);
        }

        [Fact]
        public void Equip_WarriorValidArmor_ShouldEquipMail()
        {
            //Arrange
            RPGHeroes.Heroes.Hero hero = new RPGHeroes.Heroes.Warrior("Jeff");
            RPGHeroes.Items.Item mail = new RPGHeroes.Items.Armors.Armor("Common Mail Chest", 1, ArmorType.Mail, Slot.Body, 1, 0, 0);
            RPGHeroes.Items.Slot armorSlot = mail.Slot;
            RPGHeroes.Items.Item expected = new RPGHeroes.Items.Armors.Armor("Common Mail Chest", 1, ArmorType.Mail, Slot.Body, 1, 0, 0);
            //Act
            hero.Equip(mail);
            RPGHeroes.Items.Item actual = hero.Equipment[armorSlot];
            //Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void Equip_WarriorInvalidArmor_ShouldThrowInvalidArmorException()
        {
            //Arrange
            RPGHeroes.Heroes.Hero hero = new RPGHeroes.Heroes.Warrior("Jeff");
            RPGHeroes.Items.Item cloth = new RPGHeroes.Items.Armors.Armor("Common Cloth Chest", 1, ArmorType.Cloth, Slot.Body, 1, 0, 0);
            string expected = "Invalid armor for this Hero class.";
            //Act & Assert
            Exception exception = Assert.Throws<InvalidArmorException> (() => hero.Equip(cloth));
            Assert.Equal(expected, exception.Message);
        }

        [Fact]
        public void Equip_MageValidWeapon_ShouldEquipStaff()
        {
            //Arrange
            RPGHeroes.Heroes.Hero hero = new RPGHeroes.Heroes.Mage("Jeff");
            RPGHeroes.Items.Item staff = new RPGHeroes.Items.Weapons.Weapon("Common Staff", 1, WeaponType.Staff, 2);
            RPGHeroes.Items.Slot weaponSlot = RPGHeroes.Items.Slot.Weapon;
            RPGHeroes.Items.Item expected = new RPGHeroes.Items.Weapons.Weapon("Common Staff", 1, WeaponType.Staff, 2);
            //Act
            hero.Equip(staff);
            RPGHeroes.Items.Item actual = hero.Equipment[weaponSlot];
            //Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void Equip_MageInvalidWeapon_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            RPGHeroes.Heroes.Hero hero = new RPGHeroes.Heroes.Mage("Jeff");
            RPGHeroes.Items.Item axe = new RPGHeroes.Items.Weapons.Weapon("Common Axe", 1, WeaponType.Axe, 2);
            string expected = "Invalid weapon for this Hero class.";
            //Act & Assert
            Exception exception = Assert.Throws<InvalidWeaponException>(() => hero.Equip(axe));
            Assert.Equal(expected, exception.Message);
        }

        [Fact]
        public void Equip_MageValidArmor_ShouldEquipCloth()
        {
            //Arrange
            RPGHeroes.Heroes.Hero hero = new RPGHeroes.Heroes.Mage("Jeff");
            RPGHeroes.Items.Item cloth = new RPGHeroes.Items.Armors.Armor("Common Cloth Chest", 1, ArmorType.Cloth, Slot.Body, 1, 0, 0);
            RPGHeroes.Items.Slot armorSlot = cloth.Slot;
            RPGHeroes.Items.Item expected = new RPGHeroes.Items.Armors.Armor("Common Cloth Chest", 1, ArmorType.Cloth, Slot.Body, 1, 0, 0);
            //Act
            hero.Equip(cloth);
            RPGHeroes.Items.Item actual = hero.Equipment[armorSlot];
            //Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void Equip_MageInvalidArmor_ShouldThrowInvalidArmorException()
        {
            //Arrange
            RPGHeroes.Heroes.Hero hero = new RPGHeroes.Heroes.Mage("Jeff");
            RPGHeroes.Items.Item plate = new RPGHeroes.Items.Armors.Armor("Common Plate Chest", 1, ArmorType.Plate, Slot.Body, 1, 0, 0);
            string expected = "Invalid armor for this Hero class.";
            //Act & Assert
            Exception exception = Assert.Throws<InvalidArmorException>(() => hero.Equip(plate));
            Assert.Equal(expected, exception.Message);
        }

        [Fact]
        public void Equip_RogueValidWeapon_ShouldEquipDagger()
        {
            //Arrange
            RPGHeroes.Heroes.Hero hero = new RPGHeroes.Heroes.Rogue("Jeff");
            RPGHeroes.Items.Item dagger = new RPGHeroes.Items.Weapons.Weapon("Common Dagger", 1, WeaponType.Dagger, 2);
            RPGHeroes.Items.Slot weaponSlot = RPGHeroes.Items.Slot.Weapon;
            RPGHeroes.Items.Item expected = new RPGHeroes.Items.Weapons.Weapon("Common Dagger", 1, WeaponType.Dagger, 2);
            //Act
            hero.Equip(dagger);
            RPGHeroes.Items.Item actual = hero.Equipment[weaponSlot];
            //Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void Equip_RogueInvalidWeapon_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            RPGHeroes.Heroes.Hero hero = new RPGHeroes.Heroes.Rogue("Jeff");
            RPGHeroes.Items.Item staff = new RPGHeroes.Items.Weapons.Weapon("Common Staff", 1, WeaponType.Staff, 2);
            string expected = "Invalid weapon for this Hero class.";
            //Act & Assert
            Exception exception = Assert.Throws<InvalidWeaponException>(() => hero.Equip(staff));
            Assert.Equal(expected, exception.Message);
        }

        [Fact]
        public void Equip_RangerValidWeapon_ShouldEquipBow()
        {
            //Arrange
            RPGHeroes.Heroes.Hero hero = new RPGHeroes.Heroes.Ranger("Jeff");
            RPGHeroes.Items.Item bow = new RPGHeroes.Items.Weapons.Weapon("Common Bow", 1, WeaponType.Bow, 2);
            RPGHeroes.Items.Slot weaponSlot = RPGHeroes.Items.Slot.Weapon;
            RPGHeroes.Items.Item expected = new RPGHeroes.Items.Weapons.Weapon("Common Bow", 1, WeaponType.Bow, 2);
            //Act
            hero.Equip(bow);
            RPGHeroes.Items.Item actual = hero.Equipment[weaponSlot];
            //Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void Equip_RangerInvalidWeapon_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            RPGHeroes.Heroes.Hero hero = new RPGHeroes.Heroes.Ranger("Jeff");
            RPGHeroes.Items.Item dagger = new RPGHeroes.Items.Weapons.Weapon("Common Dagger", 1, WeaponType.Dagger, 2);
            string expected = "Invalid weapon for this Hero class.";
            //Act & Assert
            Exception exception = Assert.Throws<InvalidWeaponException>(() => hero.Equip(dagger));
            Assert.Equal(expected, exception.Message);
        }

        [Fact]
        public void Create_CreateMage_ShouldHaveMageStartingHeroAttributes()
        {
            //Arrange
            RPGHeroes.Heroes.Hero mage = new RPGHeroes.Heroes.Mage("Jeff");
            HeroAttributes expected = new HeroAttributes(1, 1, 8);
            HeroAttributes actual = mage.TotalAttributes;
            //Act & Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Create_CreateWarrior_ShouldHaveWarriorStartingHeroAttributes()
        {
            //Arrange
            RPGHeroes.Heroes.Hero warrior = new RPGHeroes.Heroes.Warrior("Jeff");
            HeroAttributes expected = new HeroAttributes(5, 2, 1);
            HeroAttributes actual = warrior.TotalAttributes;
            //Act & Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Equip_MageClothChestStrength1_ShouldGain1StrengthOnEquip()
        {
            //Arrange
            RPGHeroes.Heroes.Hero mage = new RPGHeroes.Heroes.Mage("Jeff");
            Armor clothChest = new("Common Cloth Chest", 1, ArmorType.Cloth, Slot.Body, 1, 0, 0);
            HeroAttributes startingAttributes = new HeroAttributes(1, 1, 8);
            int expected = startingAttributes.Strength + 1;
            //Act
            mage.Equip(clothChest);
            int actual = mage.TotalAttributes.Strength;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Equip_MageTwoPiecesOfArmor_ShouldIncreaseTotalAttributes()
        {
            //Arrange
            RPGHeroes.Heroes.Hero mage = new RPGHeroes.Heroes.Mage("Jeff");
            int chestStrength = 4;
            int chestIntelligence = 5;
            Armor clothChest = new("Common Cloth Chest", 1, ArmorType.Cloth, Slot.Body, chestStrength, 0, chestIntelligence);
            int helmetStrength = 2;
            int helmetIntelligence = 3;
            Armor clothHelmet = new("Common Cloth Helmet", 1, ArmorType.Cloth, Slot.Head, helmetStrength, 0, helmetIntelligence);
            int startingStrength = 1;
            int startingDexterity = 1;
            int startingIntelligence = 8;
            HeroAttributes expected = new HeroAttributes(startingStrength+chestStrength+helmetStrength, startingDexterity, startingIntelligence+chestIntelligence+helmetIntelligence);
            //Act
            mage.Equip(clothChest);
            mage.Equip(clothHelmet);
            HeroAttributes actual = mage.TotalAttributes;
            //Assert
            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void LevelUp_LevelUpMage_ShouldIncreaseLevelByOne()
        {
            //Arrange
            RPGHeroes.Heroes.Hero mage = new RPGHeroes.Heroes.Mage("Jeff");
            int startingLevel = 1;
            int expected = startingLevel+1;
            //Act
            mage.LevelUp();
            int actual = mage.Level;
            //Arrange
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_LevelUpMage_ShouldIncreaseLevelAttributesBy1Strength1Dexterity5Intelligence()
        {
            //Arrange
            RPGHeroes.Heroes.Hero mage = new RPGHeroes.Heroes.Mage("Jeff");
            int startingStrength = 1;
            int startingDexterity = 1;
            int startingIntelligence = 8;
            HeroAttributes expected = new HeroAttributes(startingStrength+1, startingDexterity+1, startingIntelligence+5);
            //Act
            mage.LevelUp();
            HeroAttributes actual = mage.TotalAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_LevelUpWarrior_ShouldIncreaseLevelAttributesBy3Strength2Dexterity1Intelligence()
        {
            //Arrange
            RPGHeroes.Heroes.Hero warrior = new RPGHeroes.Heroes.Warrior("Jeff");
            int startingStrength = 5;
            int startingDexterity = 2;
            int startingIntelligence = 1;
            HeroAttributes expected = new HeroAttributes(startingStrength + 3, startingDexterity + 2, startingIntelligence + 1);
            //Act
            warrior.LevelUp();
            HeroAttributes actual = warrior.TotalAttributes;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Create_CreateMage_ShouldHaveHeroDamageBasedOnDamageFormulaWithIntelligence()
        {
            //Arrange
            RPGHeroes.Heroes.Hero mage = new RPGHeroes.Heroes.Mage("Jeff");
            int startingWeaponDamage = 1;
            int startingIntelligence = 8;
            int expected = startingWeaponDamage * (1 + startingIntelligence / 100);
            //Act
            int actual = mage.Damage;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Equip_EquipWeaponMage_ShouldIncreaseHeroDamageBasedOnDamageFormula()
        {
            //Arrange
            RPGHeroes.Heroes.Hero mage = new RPGHeroes.Heroes.Mage("Jeff");
            int weaponDamage = 2;
            int startingIntelligence = 8;
            RPGHeroes.Items.Item staff = new RPGHeroes.Items.Weapons.Weapon("Common Staff", 1, WeaponType.Staff, weaponDamage);
            int expected = weaponDamage * (1 + startingIntelligence / 100);
            //Act
            mage.Equip(staff);
            int actual = mage.Damage;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Equip_EquipWeaponWarrior_ShouldIncreaseHeroDamageBasedOnDamageFormula()
        {
            //Arrange
            RPGHeroes.Heroes.Hero warrior = new RPGHeroes.Heroes.Warrior("Jeff");
            int weaponDamage = 2;
            int startingStrength = 5;
            RPGHeroes.Items.Item axe = new RPGHeroes.Items.Weapons.Weapon("Common Axe", 1, WeaponType.Axe, weaponDamage);
            int expected = weaponDamage * (1 + startingStrength / 100);
            //Act
            warrior.Equip(axe);
            int actual = warrior.Damage;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Equip_EquipWeaponArmorMage_ShouldIncreaseHeroDamageBasedOnDamageFormula()
        {
            //Arrange
            RPGHeroes.Heroes.Hero mage = new RPGHeroes.Heroes.Mage("Jeff");
            int weaponDamage = 2;
            int startingIntelligence = 8;
            int armorIntelligence = 2;
            RPGHeroes.Items.Item staff = new RPGHeroes.Items.Weapons.Weapon("Common Staff", 1, WeaponType.Staff, weaponDamage);
            Armor clothChest = new("Magic Cloth Chest", 1, ArmorType.Cloth, Slot.Body, 0, 0, armorIntelligence);
            int expected = weaponDamage * (1 + (startingIntelligence + armorIntelligence) / 100);
            //Act
            mage.Equip(staff);
            mage.Equip(clothChest);
            int actual = mage.Damage;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Equip_EquipWeaponArmorWarrior_ShouldIncreaseHeroDamageBasedOnDamageFormula()
        {
            //Arrange
            RPGHeroes.Heroes.Hero warrior = new RPGHeroes.Heroes.Warrior("Jeff");
            int weaponDamage = 2;
            int startingStrength = 5;
            int armorStrength = 10;
            RPGHeroes.Items.Item axe = new RPGHeroes.Items.Weapons.Weapon("Common Axe", 1, WeaponType.Axe, weaponDamage);
            Armor clothChest = new("Magic Mail Chest", 1, ArmorType.Mail, Slot.Body, armorStrength, 0, 0);
            int expected = weaponDamage * (1 + (startingStrength + armorStrength) / 100);
            //Act
            warrior.Equip(axe);
            int actual = warrior.Damage;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Equip_ReplaceWeaponWarrior_ShouldIncreaseHeroDamageBasedOnDamageFormulaAndCurrentWeapon()
        {
            //Arrange
            RPGHeroes.Heroes.Hero warrior = new RPGHeroes.Heroes.Warrior("Jeff");
            int weaponDamage = 6;
            int startingStrength = 5;
            RPGHeroes.Items.Item axe = new RPGHeroes.Items.Weapons.Weapon("Common Axe", 1, WeaponType.Axe, 1);
            RPGHeroes.Items.Item sword = new RPGHeroes.Items.Weapons.Weapon("Common Axe", 1, WeaponType.Axe, weaponDamage);
            int expected = weaponDamage * (1 + startingStrength / 100);
            //Act
            warrior.Equip(axe);
            warrior.Equip(sword);
            int actual = warrior.Damage;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Equip_ReplaceClothChestMage_ShouldGainOnly5StrengthOnLastEquip()
        {
            //Arrange
            RPGHeroes.Heroes.Hero mage = new RPGHeroes.Heroes.Mage("Jeff");
            int strengthIncrease = 5;
            Armor clothChest = new("Common Cloth Chest", 1, ArmorType.Cloth, Slot.Body, 1, 0, 0);
            Armor magicClothChest = new("Magic Cloth Chest", 1, ArmorType.Cloth, Slot.Body, strengthIncrease, 0, 0);
            HeroAttributes startingAttributes = new HeroAttributes(1, 1, 8);
            int expected = startingAttributes.Strength + strengthIncrease;
            //Act
            mage.Equip(clothChest);
            mage.Equip(magicClothChest);
            int actual = mage.TotalAttributes.Strength;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Display_HeroDisplay_ShouldReturnCorrectlyFormattedString()
        {
            //Arrange
            RPGHeroes.Heroes.Hero mage = new RPGHeroes.Heroes.Mage("Jeff");
            string name = "Jeff";
            string heroClass = "Mage";
            int level = 1;
            int strengthLevel = 1;
            int dexterityLevel = 1;
            int intelligenceLevel = 8;
            int damage = 1 * (1 + intelligenceLevel / 100);
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Name: {0}\n", name)
                .AppendFormat("Class: {0}\n", heroClass)
                .AppendFormat("Level: {0}\n", level)
                .Append("Stats:\n")
                .AppendFormat("Total strength: {0}\n", strengthLevel)
                .AppendFormat("Total dexterity: {0}\n", dexterityLevel)
                .AppendFormat("Total intelligence: {0}\n", intelligenceLevel)
                .AppendFormat("Damage: {0}\n", damage);
            string expected = sb.ToString();
            //Act
            string actual = mage.Display();
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}