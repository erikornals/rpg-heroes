using RPGHeroes.Attributes;
using RPGHeroes.Items;
using RPGHeroes.Items.Armors;

namespace RPGHeroesTests
{
    public class ArmorTests
    {
        [Fact]
        public void Create_PlateChest_ShouldHaveNameCommonPlateChest()
        {
            //Arrange
            Armor plateChest = new("Common Plate Chest", 1, ArmorType.Plate, Slot.Body, 1, 0, 0);
            string expected = "Common Plate Chest";
            //Act & Assert
            Assert.Equal(plateChest.Name, expected);
        }

        [Fact]
        public void Create_PlateChest_ShouldHaveRequiredLevel1()
        {
            //Arrange
            Armor plateChest = new("Common Plate Chest", 1, ArmorType.Plate, Slot.Body, 1, 0, 0);
            int expected = 1;
            //Act & Assert
            Assert.Equal(plateChest.RequiredLevel, expected);
        }

        [Fact]
        public void Create_PlateChest_ShouldHaveArmorTypePlate()
        {
            //Arrange
            Armor plateChest = new("Common Plate Chest", 1, ArmorType.Plate, Slot.Body, 1, 0, 0);
            ArmorType expected = ArmorType.Plate;
            //Act & Assert
            Assert.Equal(plateChest.ArmorType, expected);
        }

        [Fact]
        public void Create_PlateChest_ShouldHaveSlotChest()
        {
            //Arrange
            Armor plateChest = new("Common Plate Chest", 1, ArmorType.Plate, Slot.Body, 1, 0, 0);
            Slot expected = Slot.Body;
            //Act & Assert
            Assert.Equal(plateChest.Slot, expected);
        }

        [Fact]
        public void Create_PlateChest_ShouldHaveArmorAttributeStrength1()
        {
            //Arrange
            Armor plateChest = new("Common Plate Chest", 1, ArmorType.Plate, Slot.Body, 1, 0, 0);
            HeroAttributes expected = new HeroAttributes(1, 0, 0);
            //Act & Assert
            Assert.Equal(plateChest.ArmorAttribute, expected);
        }
    }
}