using RPGHeroes.Items.Weapons;

namespace RPGHeroesTests
{
    public class WeaponTests
    {
        [Fact]
        public void Create_Axe_ShouldHaveNameCommonAxe()
        {
            //Arrange
            Weapon axe = new RPGHeroes.Items.Weapons.Weapon("Common Axe", 1, WeaponType.Axe, 2);
            string expected = "Common Axe";
            //Act & Assert
            Assert.Equal(axe.Name, expected);
        }

        [Fact]
        public void Create_Axe_ShouldHaveSlotWeapon()
        {
            //Arrange
            Weapon axe = new RPGHeroes.Items.Weapons.Weapon("Common Axe", 1, WeaponType.Axe, 2);
            RPGHeroes.Items.Slot expected = RPGHeroes.Items.Slot.Weapon;
            //Act & Assert
            Assert.Equal(axe.Slot, expected);
        }

        [Fact]
        public void Create_Axe_ShouldHaveRequiredLevel1()
        {
            //Arrange
            Weapon axe = new RPGHeroes.Items.Weapons.Weapon("Common Axe", 1, WeaponType.Axe, 2);
            int expected = 1;
            //Act & Assert
            Assert.Equal(axe.RequiredLevel, expected);
        }

        [Fact]
        public void Create_Axe_ShouldHaveWeaponTypeAxe()
        {
            //Arrange
            Weapon axe = new RPGHeroes.Items.Weapons.Weapon("Common Axe", 1, WeaponType.Axe, 2);
            WeaponType expected = WeaponType.Axe;
            //Act & Assert
            Assert.Equal(axe.WeaponType, expected);
        }

        [Fact]
        public void Create_Axe_ShouldHaveWeaponDamage2()
        {
            //Arrange
            Weapon axe = new RPGHeroes.Items.Weapons.Weapon("Common Axe", 1, WeaponType.Axe, 2);
            int expected = 2;
            //Act & Assert
            Assert.Equal(axe.WeaponDamage, expected);
        }
    }
}