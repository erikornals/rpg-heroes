﻿namespace RPGHeroes.Attributes
{
    public struct HeroAttributes
    {
        /// <summary>
        /// HeroAttributes structure.
        /// </summary>

        public int Strength;
        public int Dexterity;
        public int Intelligence;

        public HeroAttributes(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        public HeroAttributes Add(HeroAttributes other) {
            return new HeroAttributes(Strength + other.Strength, Dexterity+other.Dexterity, Intelligence+other.Intelligence);
        }
        
        public override string ToString()
        {
            return $"Strength: {Strength}, Dexterity: {Dexterity}, Intelligence: {Intelligence}";
        }
    }
}
