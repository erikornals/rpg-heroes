﻿using RPGHeroes.Attributes;

namespace RPGHeroes.Items.Armors
{
    public class Armor : Item
    {
        /// <summary>
        /// Armor class. Subclass of Item.
        /// </summary>
        
        public ArmorType ArmorType { get; set; }
        public HeroAttributes ArmorAttribute { get; set; }
        public Armor(string name, int requiredLevel, ArmorType armorType, Slot armorSlot, int strength, int dexterity, int intelligence) : base(name, requiredLevel)
        {
            ArmorType = armorType;
            Slot = armorSlot;
            ArmorAttribute = new HeroAttributes(strength, dexterity, intelligence);
        }
    }
}
