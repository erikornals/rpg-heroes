﻿namespace RPGHeroes.Items.Armors
{
    public enum ArmorType
    {
        /// <summary>
        /// Armor type enum.
        /// </summary>
        
        Cloth,
        Leather,
        Mail,
        Plate
    }
}
