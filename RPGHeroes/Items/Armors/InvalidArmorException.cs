﻿namespace RPGHeroes.Items.Armors
{
    public class InvalidArmorException: Exception
    {
        /// <summary>
        /// InvalidArmor exception class.
        /// </summary>
        
        public InvalidArmorException() { }

        public InvalidArmorException(string message) : base(message) { }

        public override string Message => "Invalid armor for this Hero class.";
    }
}
