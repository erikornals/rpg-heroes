﻿namespace RPGHeroes.Items
{
    public abstract class Item
    {
        /// <summary>
        /// Abstract Item class.
        /// </summary>
        public string Name { get; set; }
        public int RequiredLevel { get; set; }
        public Slot Slot { get; set; }

        public Item(string name, int requiredLevel)
        {
            Name = name;
            RequiredLevel = requiredLevel;
        }
    }
}
