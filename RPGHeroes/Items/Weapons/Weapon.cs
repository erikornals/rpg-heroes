﻿namespace RPGHeroes.Items.Weapons
{
    public class Weapon : Item
    {
        /// <summary>
        /// Weapon class. Subclass of Item.
        /// </summary>
        
        public WeaponType WeaponType { get; set; }
        public int WeaponDamage { get; set; }
        public Weapon(string name, int requiredLevel, WeaponType weaponType, int weaponDamage) : base(name, requiredLevel)
        {
            Slot = Slot.Weapon;
            WeaponType = weaponType;
            WeaponDamage = weaponDamage;
        }
    }
}
