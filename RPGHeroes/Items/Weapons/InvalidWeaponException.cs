﻿namespace RPGHeroes.Items.Weapons
{
    public class InvalidWeaponException: Exception
    {
        /// <summary>
        /// Invalid weapon exception class.
        /// </summary>
        
        public InvalidWeaponException() { }

        public InvalidWeaponException(string message) : base(message) { }

        public override string Message => "Invalid weapon for this Hero class.";
    }
}
