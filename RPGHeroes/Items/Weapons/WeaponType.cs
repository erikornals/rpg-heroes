﻿namespace RPGHeroes.Items.Weapons
{
    public enum WeaponType
    {
        /// <summary>
        /// Weapon type enum.
        /// </summary>
        
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }
}
