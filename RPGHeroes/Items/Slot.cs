﻿namespace RPGHeroes.Items
{
    public enum Slot
    {
        /// <summary>
        /// Slot Enum type.
        /// </summary>
        Weapon,
        Head,
        Body,
        Legs
    }
}