﻿using RPGHeroes.Attributes;
using RPGHeroes.Items.Armors;
using RPGHeroes.Items.Weapons;

namespace RPGHeroes.Heroes
{
    public class Ranger : Hero
    {
        /// <summary>
        /// Ranger class. Subclass of Hero.
        /// </summary>
        /// <param name="name"></param>
        public Ranger(string name) : base(name)
        {
            ValidWeaponTypes.Add(WeaponType.Bow);
            ValidArmorTypes.Add(ArmorType.Leather);
            ValidArmorTypes.Add(ArmorType.Mail);

            LevelAttributes = new HeroAttributes(1, 7, 1);
            AttributeGain = new HeroAttributes(1, 5, 1);

            DamagingAttribute = TotalAttributes.Dexterity;
        }
    }
}
