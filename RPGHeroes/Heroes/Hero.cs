﻿using RPGHeroes.Items;
using RPGHeroes.Items.Armors;
using RPGHeroes.Items.Weapons;
using RPGHeroes.Attributes;
using System.Text;

namespace RPGHeroes.Heroes
{
    public abstract class Hero
    {
        /// <summary>
        /// Abstract Hero superclass.
        /// </summary>
        /// 

        //Public properties
        public string Name { get; set; }
        public int Level { get; set; }

        public Dictionary<Slot, Item> Equipment = new Dictionary<Slot, Item>
        {
            {Slot.Weapon, null },
            {Slot.Head, null },
            {Slot.Body, null },
            {Slot.Legs, null },
        };
        public HeroAttributes TotalAttributes { get => CalculateTotalAttributes(); }
        public int Damage
        {
            get => WeaponDamage * (1 + DamagingAttribute / 100);
        }

        //Protected properties
        protected List<WeaponType> ValidWeaponTypes = new List<WeaponType>();
        protected List<ArmorType> ValidArmorTypes = new List<ArmorType>();
        protected HeroAttributes LevelAttributes { get; set; }
        protected HeroAttributes AttributeGain { get; set; }
        protected int DamagingAttribute { get; set; }

        //Private properties
        private int WeaponDamage 
        {
            get {
                if (Equipment[Slot.Weapon] is not null)
                {
                    Weapon weapon = (Weapon) Equipment[Slot.Weapon];
                    return weapon.WeaponDamage;
                }
                return 1;
            }
        }

        //Public methods
        public Hero(string name) {
            Name = name;
            Level = 1;
        }

        public virtual void LevelUp()
        {
            LevelAttributes = LevelAttributes.Add(AttributeGain);
            Level += 1;
        }

        public void Equip(Item item)
        {
            if (item.GetType() == typeof(Weapon))
            {
                EquipWeapon((Weapon) item);
            }
            if (item.GetType() == typeof(Armor))
            {
                EquipArmor((Armor)item);
            }
            Equipment[item.Slot] = item;
        }

        public string Display()
        {
            HeroAttributes totalAttributes = CalculateTotalAttributes();
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Name: {0}\n", Name)
                .AppendFormat("Class: {0}\n", this.GetType().Name)
                .AppendFormat("Level: {0}\n", Level)
                .Append("Stats:\n")
                .AppendFormat("Total strength: {0}\n", totalAttributes.Strength)
                .AppendFormat("Total dexterity: {0}\n", totalAttributes.Strength)
                .AppendFormat("Total intelligence: {0}\n", totalAttributes.Intelligence)
                .AppendFormat("Damage: {0}\n", Damage);
            return sb.ToString();
        }

        //Private methods
        private void EquipWeapon(Weapon weapon)
        {
            if (!ValidWeaponTypes.Contains(weapon.WeaponType))
            {
                throw new InvalidWeaponException();
            }
            Equipment[Slot.Weapon] = weapon;
        }

        private void EquipArmor(Armor armor)
        {
            if (!ValidArmorTypes.Contains(armor.ArmorType))
            {
                throw new InvalidArmorException();
            }
            Equipment[armor.Slot] = armor;
        }

        private HeroAttributes CalculateTotalAttributes()
        {
            HeroAttributes totalAttributes = LevelAttributes;

            foreach (KeyValuePair<Slot, Item> entry in Equipment)
            {
                Armor armor = (Armor)entry.Value;
                if (entry.Key != Slot.Weapon && armor is not null)
                {
                    totalAttributes = totalAttributes.Add(armor.ArmorAttribute);
                }
            }
            return totalAttributes;
        }
    }
}
