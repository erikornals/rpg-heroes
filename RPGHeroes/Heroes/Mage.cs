﻿using RPGHeroes.Attributes;
using RPGHeroes.Items.Armors;
using RPGHeroes.Items.Weapons;

namespace RPGHeroes.Heroes
{
    public class Mage : Hero
    {
        /// <summary>
        /// Mage class. Subclass of Hero.
        /// </summary>
        /// <param name="name"></param>

        public Mage(string name) : base(name){
            ValidWeaponTypes.Add(WeaponType.Staff);
            ValidWeaponTypes.Add(WeaponType.Wand);
            ValidArmorTypes.Add(ArmorType.Cloth);

            LevelAttributes = new HeroAttributes(1, 1, 8);
            AttributeGain = new HeroAttributes(1, 1, 5);

            DamagingAttribute = TotalAttributes.Intelligence;
        }
    }
}
