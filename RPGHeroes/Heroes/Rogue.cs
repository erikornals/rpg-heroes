﻿using RPGHeroes.Attributes;
using RPGHeroes.Items.Armors;
using RPGHeroes.Items.Weapons;

namespace RPGHeroes.Heroes
{
    public class Rogue: Hero
    {
        /// <summary>
        /// Rogue class. Subclass of Hero.
        /// </summary>
        /// <param name="name"></param>
        public Rogue(string name) : base(name)
        {
            ValidWeaponTypes.Add(WeaponType.Dagger);
            ValidWeaponTypes.Add(WeaponType.Sword);
            ValidArmorTypes.Add(ArmorType.Leather);
            ValidArmorTypes.Add(ArmorType.Mail);

            LevelAttributes = new HeroAttributes(2, 6, 1);
            AttributeGain = new HeroAttributes(1, 4, 1);

            DamagingAttribute = TotalAttributes.Dexterity;
        }
    }
}
