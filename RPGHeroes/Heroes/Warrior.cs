﻿using RPGHeroes.Attributes;
using RPGHeroes.Items.Armors;
using RPGHeroes.Items.Weapons;

namespace RPGHeroes.Heroes
{
    public class Warrior : Hero
    {

        /// <summary>
        /// Warrior class. Subclass of Hero.
        /// </summary>
        /// <param name="name"></param>


        public Warrior(string name) : base(name) {
            ValidWeaponTypes.Add(WeaponType.Axe);
            ValidWeaponTypes.Add(WeaponType.Hammer);
            ValidWeaponTypes.Add(WeaponType.Sword);
            ValidArmorTypes.Add(ArmorType.Mail);
            ValidArmorTypes.Add(ArmorType.Plate);

            LevelAttributes = new HeroAttributes(5, 2, 1);
            AttributeGain = new HeroAttributes(3, 2, 1);

            DamagingAttribute = TotalAttributes.Strength;
        }
    }
}
