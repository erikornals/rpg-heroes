# RPGHeroes

RPGHeroes is a library of C# for simulating a simple RPG.

## Installation

Install Visual Studios and clone the repository as a new solution.

## Usage

Run all tests (Ctrl + R, A) to check functionality of classes.

## Contributing
Erik Alstad